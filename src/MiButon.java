
import javax.swing.JButton;
import javax.swing.JFrame;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

public class MiButon extends JFrame {

    private JButton jb;

    MiButon() {
        initComponents();
    }

    private void initComponents() {
        setSize(100, 100);
        jb = new JButton();
        add(jb);
        jb.setText("Boton");

        setSize(500, 200); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        setVisible(true); // Ver el JFrame
    }

    public static void main(String args[]) {
        new MiButon();
    }

}
