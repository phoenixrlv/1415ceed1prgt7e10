
import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paco Aldarias<paco.aldarias@gmail.com>
 * @author 04-feb-2015
 */
public class MiBorderLayout extends JFrame {

   private JLabel norte;
   private JLabel sur;
   private JLabel este;
   private JLabel oeste;
   private JLabel centro;
   private BorderLayout b;

   public MiBorderLayout() {

      norte = new JLabel("Norte");
      sur = new JLabel("Sur");
      este = new JLabel("Este");
      oeste = new JLabel("Oeste");
      centro = new JLabel("Centro");

      add(norte, BorderLayout.NORTH);
      add(oeste, BorderLayout.WEST);
      add(este, BorderLayout.EAST);
      add(sur, BorderLayout.SOUTH);
      add(centro, BorderLayout.CENTER);

      b = new BorderLayout();
      setLayout(b);
      b.setVgap(20);
      //b.setHgap(20);

      //algunas configuraciones de la ventana
      //setSize(300, 200);
      pack();
      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      this.setLocationRelativeTo(null); // Centrar
      this.setVisible(true);

   }

   public static void main(String[] argc) {
      new MiBorderLayout();
   }

}
