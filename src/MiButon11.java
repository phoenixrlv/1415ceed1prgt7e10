
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MiButon11 extends JFrame {

    private JButton jb;
    private JLabel jl;
    private JPanel jp;
    private int contador = 0;

    MiButon11() {
        initComponents();
    }

    private void initComponents() {

        // Panel
        jp = new JPanel();
        Dimension d = new Dimension(500, 500);
        jp.setSize(d);
        add(jp);

        // Label
        jl = new JLabel("Etiqueta");
        jp.add(jl);

        jb = new JButton("Boton");

        // Button
        jp.add(jb);

        ActionListener MiActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String comando = e.getActionCommand();
                getjl().setText("Pulsado " + comando + " " + getContador());
            }
        };
        jb.addActionListener(MiActionListener);
        jb.setActionCommand("Boton");

        // Frame
        setLayout(new GridLayout(0, 1));
        //setSize(600, 500); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        pack();
    }

    public static void main(String args[]) {
        new MiButon11().setVisible(true);
    }

    public JLabel getjl() {
        return jl;
    }

    public int getContador() {
        contador++;
        return contador;
    }
}
