
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author paco
 */
public class ActionListener1 extends JFrame {

    private JButton b;

    public ActionListener1() {
        super("ActionListener1");
        b = new JButton("Pulsame");
        ActionListener1a elListener = new ActionListener1a();
        b.addActionListener(elListener);
        add(b);

        //pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        setSize(250, 100);
    }

    public static void main(String[] argc) {
        new ActionListener1().setVisible(true);
    }

}
