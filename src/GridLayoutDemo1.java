
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paco Aldarias<paco.aldarias@gmail.com>
 * @author 04-feb-2015
 */
public class GridLayoutDemo1 extends JFrame {

    private JPanel p1;
    private JPanel p2;
    private JPanel p3;

    private JLabel l1;
    private JLabel l2;

    private JTextField t1;
    private JPasswordField t2;

    public GridLayoutDemo1() {

        GridLayout gl = new GridLayout(2, 1, 10, 10);
        BorderLayout bl = new BorderLayout();
        FlowLayout fl = new FlowLayout();

        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();

        //gl.setRows(2);
        //gl.setHgap(30);
        //gl.setColumns(1);
        //gl.setVgap(20);
        p1.setLayout(gl);
        p2.setLayout(gl);
        p3.setLayout(bl);

        //  setBackground(Color.yellow);
        p1.setBackground(Color.pink);
        p2.setBackground(Color.red);

        l1 = new JLabel("User");
        l2 = new JLabel("Password");

        t1 = new JTextField("Paco");
        t1.setColumns(15);

        t2 = new JPasswordField();
        t2.setColumns(15);

        add(p3);

        p3.add(p1, bl.WEST);
        p3.add(p2, bl.CENTER);

        p1.add(l1);
        p1.add(l2);

        p2.add(t1);
        p2.add(t2);

        //algunas configuraciones de la ventana
        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setLocationRelativeTo(null); // Centrar
        //setSize(200, 300); // Ancho x Alto
        setVisible(true);

    }

    public static void main(String[] argc) {
        new GridLayoutDemo1();
    }

}
