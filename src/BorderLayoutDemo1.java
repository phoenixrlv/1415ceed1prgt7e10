
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paco Aldarias<paco.aldarias@gmail.com>
 * @author 04-feb-2015
 */
public class BorderLayoutDemo1 extends JFrame {

    Button btnOeste = new Button();
    Button btnEste = new Button();
    Button btnNorte = new Button();
    Button btnSur = new Button();
    Button btnCentro = new Button();
    BorderLayout borderLayout1 = new BorderLayout();

    public BorderLayoutDemo1() {

        this.setSize(new Dimension(336, 253));
        this.setLayout(borderLayout1);

        btnOeste.setLabel("Oeste");
        btnEste.setLabel("Este");
        btnNorte.setLabel("Norte");
        btnSur.setLabel("Sur");
        btnCentro.setLabel("Centro");

        //borderLayout1.setVgap(20);
        borderLayout1.setHgap(20);

        this.add(btnOeste, BorderLayout.WEST);
        this.add(btnEste, BorderLayout.EAST);
        this.add(btnNorte, BorderLayout.NORTH);
        this.add(btnSur, BorderLayout.SOUTH);
        this.add(btnCentro, BorderLayout.CENTER);

        //algunas configuraciones de la ventana
        this.pack();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null); // Centrar
        this.setVisible(true);

    }

    public static void main(String[] argc) {
        new BorderLayoutDemo1();
    }

}
