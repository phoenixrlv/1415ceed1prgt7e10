
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

class MiActionListener1 implements ActionListener {

    MiButon1 vista;

    MiActionListener1(MiButon1 v) {
        vista = v;
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String comando = evento.getActionCommand();
        vista.getjl().setText("Pulsado " + comando + " " + vista.getContador());
    }
}

public class MiButon1 extends JFrame {

    private JButton jb;
    private JLabel jl;
    private JPanel jp;
    private int contador = 0;

    MiButon1() {
        initComponents();
    }

    private void initComponents() {

        jp = new JPanel();
        Dimension d = new Dimension(500, 500);
        jp.setSize(d);

        jl = new JLabel("Etiqueta");
        jb = new JButton("Boton");

        setLayout(new GridLayout());

        add(jp);
        jp.add(jb);
        jp.add(jl);

        jb.setActionCommand("Boton");
        ActionListener miactionlistener = new MiActionListener1(this);
        jb.addActionListener(miactionlistener);

        setSize(600, 600); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        setVisible(true); // Ver el JFrame
        pack();
    }

    public static void main(String args[]) {
        MiButon1 v = new MiButon1();
        MiActionListener1 c = new MiActionListener1(v);
    }

    public JLabel getjl() {
        return jl;
    }

    public int getContador() {
        contador++;
        return contador;
    }
}
