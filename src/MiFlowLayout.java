/*
 *
 * FlowLayoutDemo.java
 *
 */

import java.awt.Button;
import java.awt.FlowLayout;
import javax.swing.JFrame;

public class MiFlowLayout extends JFrame {

    public MiFlowLayout() {

        Button button1 = new Button("Ok");
        Button button2 = new Button("Open");
        Button button3 = new Button("Close");
        add(button1);
        add(button2);
        add(button3);

        // Frame
        setLayout(new FlowLayout());
        setSize(100, 100); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        //pack();

    }

    public static void main(String args[]) {
        new MiFlowLayout().setVisible(true);
    }
}
