
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MiJLabel2 {

    JLabel l = new JLabel();

    MiJLabel2() {
        initComponents();

    }

    private void initComponents() {

        JFrame f = new JFrame();

        f.setTitle("Ejemplo JLabel");
        f.setLayout(new GridLayout());
        f.add(l);
        l.setText("Texto del JLabel");

        f.setLocationRelativeTo(null); // Centrar
        f.setSize(250, 100); // Columnas 250  x Filas 100
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        f.setVisible(true);

    }

    public static void main(String args[]) {
        new MiJLabel2();
    }

}
