
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

class CerrarVentana extends JFrame {

    public CerrarVentana() {
        initComponents();
    }

    private void initComponents() {
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Aplicacion Hola Mundo");
        label = new JLabel("Hola Mundo");
        getContentPane().add(label);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                accionCerrar(e);
            }
        });
        setSize(200, 100);
        setLocationRelativeTo(null); // Centrar
        setVisible(true);
    }

    private void accionCerrar(java.awt.event.WindowEvent evt) {
        int op = finalizar();
        if (op == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }

    private int finalizar() {
        return JOptionPane.showConfirmDialog(this, "Seguro que quiere salir?");
    }

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CerrarVentana().setVisible(true);
            }
        });
    }
    private JLabel label;
}
