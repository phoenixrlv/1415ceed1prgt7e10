
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ActionListener3 extends JFrame implements ActionListener {

    private JButton b;

    public ActionListener3() {
        super("ActionListener2");
        b = new JButton("Pulsame");
        b.addActionListener(this);
        add(b);

        pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
    }

    private void pulsado() {
        b.setText("Pulsado");
    }

    public void actionPerformed(ActionEvent e) {
        pulsado();
    }

    public static void main(String[] argc) {
        new ActionListener2().setVisible(true);
    }

}
