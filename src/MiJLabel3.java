
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MiJLabel3 {

    MiJLabel3() {
        initComponents();
    }

    private void initComponents() {
        JFrame f = new JFrame();
        JPanel p = new JPanel();
        JLabel l = new JLabel();
        f.setTitle("Ejemplo JLabel");
        f.setLayout(new GridLayout());
        f.add(p);
        f.setSize(230, 100);
        f.setLocationRelativeTo(null); // Centrar
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        f.setVisible(true);
        p.add(l);
        l.setText("Texto del JLabel");
    }

    public static void main(String args[]) {
        new MiJLabel3();
    }

}
