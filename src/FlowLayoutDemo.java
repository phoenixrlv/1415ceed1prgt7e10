/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Paco Aldarias<paco.aldarias@gmail.com>
 * @author 04-feb-2015 Fuente:
 * https://inforux.wordpress.com/2009/01/19/java-practicando-con-flowlayout/
 */
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.JFrame;

public class FlowLayoutDemo {

    private JButton botonEnviar;
    private JTextField cajaTexto;
    private JLabel etiqueta;

    public void construyeVentana() {
        //creamos la ventana y asignamos el layout
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());

        //Creamos los componentes a utilizar
        botonEnviar = new JButton("Enviar");
        cajaTexto = new JTextField(12);
        etiqueta = new JLabel("Escribe tu nombre");

        //Aniadimos los componentes  a la ventana
        frame.add(etiqueta);
        frame.add(cajaTexto);
        frame.add(botonEnviar);

        //algunas configuraciones de la ventana
        frame.pack();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null); // Centrar
        frame.setVisible(true);
    }

    public FlowLayoutDemo() {//constructor de la clase
        construyeVentana();
    }

    public static void main(String[] argc) {
        new FlowLayoutDemo();
    }
}
