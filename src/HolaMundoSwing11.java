
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HolaMundoSwing11 {

    public HolaMundoSwing11() {

        JFrame f = new JFrame("HolaMundoSwing11");
        JLabel label = new JLabel("Hola Mundo");
        f.add(label);

        f.setSize(500, 200); // Tamaño de la ventana
        f.setLocationRelativeTo(null); // Centrar
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        f.setVisible(true); // Ver el JFrame

    }

    public static void main(String[] args) {
        new HolaMundoSwing11();
    }
}
