
# Bash Script 
# Por Paco Aldarias.

rm *.java

cp ~/Dropbox/ceed1314/apuntes/NetBeansProjects/t9/prgt9e10aldarias/src/prgt9e10aldarias/* .

echo "Renombrando ficheros con Ejemplo09 por Ejemplo08"
rename -v 's/Ejemplo09/Ejemplo07/' *.java

for i in $(ls *.java);do 
 echo $i
 
 echo "Borrando lineas con el texto package"
 cat $i | grep  -Ev package > t$i 
 mv t$i $i
 
 echo "Reemplazando el texto Ejemplo07 por Ejemplo07"
 cat $i | sed 's/Ejemplo09/Ejemplo07/g' > t$i
 mv  t$i $i
done   
