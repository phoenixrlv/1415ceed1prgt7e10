
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ActionListener5 extends JFrame implements ActionListener {

    private JButton b1;
    private JButton b2;

    public ActionListener5() {
        super("ActionListener5");

        setLayout(new FlowLayout());

        b1 = new JButton("Pulsame");
        b1.addActionListener(this);
        add(b1);

        b2 = new JButton("Salir");
        b2.addActionListener(this);
        add(b2);

        pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
    }

    private void metodo1() {
        b1.setText("Pulsado");
    }

    private void metodo2() {
        System.exit(0); // Cierra la aplicación
    }

    public void actionPerformed(ActionEvent e) {
        Object fuente = e.getSource();

        if (fuente == b1) {
            metodo1();
        } else if (fuente == b2) {
            metodo2();
        }

    }

    public static void main(String[] argc) {
        new ActionListener5().setVisible(true);
    }

}
