
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HolaMundoSwing extends JFrame {

    public HolaMundoSwing() {
        super("Hola Mundo"); // Titulo
        JLabel label = new JLabel("Hola Mundo");
        add(label);

        setSize(200, 100); // Tamaño de la ventana
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        //setVisible(true); // Ver el JFrame
        //pack(); // Ajustar al marco
    }

    public static void main(String[] args) {
        new HolaMundoSwing().setVisible(true);
    }
}
