
import javax.swing.JFrame;
import javax.swing.JLabel;

public class HolaMundoSwing1 extends JFrame {

    public HolaMundoSwing1() {
        //setTitle("Titulo");
        super("Titulo");
        JLabel l = new JLabel("Hola Mundo");
        add(l);
        setVisible(true);
        pack();
    }

    public static void main(String[] args) {
        new HolaMundoSwing1();
    }
}
