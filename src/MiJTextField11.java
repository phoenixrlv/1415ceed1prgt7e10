
import java.awt.Color;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

// Ejemplo con dos paneles y FlowLayout
public class MiJTextField11 {

    private JFrame f;

    private JPanel p;
    private JPanel p1;

    private JTextField t;
    private JLabel l;

    private JTextField t1;
    private JLabel l1;

    MiJTextField11() {

        f = new JFrame();

        p = new JPanel();
        p1 = new JPanel();

        l = new JLabel();
        t = new JTextField();

        l1 = new JLabel();
        t1 = new JTextField();

        f.add(p);
        p.add(l);
        p.add(t);

        f.add(p1);
        p1.add(l1);
        p1.add(t1);

        l.setText("Nombre: ");
        t.setText("Pepe");
        t.setColumns(15);
        p.setBackground(Color.yellow);

        l1.setText("Edad: ");
        t1.setText("12");
        t1.setColumns(5);
        p1.setBackground(Color.blue);

        f.setLayout(new FlowLayout());
        f.setLocationRelativeTo(null); // Centrar
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        f.setSize(300, 100);
        f.setVisible(true);
        //f.pack();
    }

    public static void main(String args[]) {
        new MiJTextField11();
    }

}
