/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing17DefaultTableModelMVC;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * Fichero: Vista.java
 * @date 09-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Vista extends JFrame 
{
	JButton btnClear = new JButton("cambiar");
	JScrollPane spFichas;
	JTable jtbFichas;
	private Modelo m_model;
	
	Vista(Modelo model) 
	{
		m_model = model;

		jtbFichas = new JTable(m_model.getModelo());
		jtbFichas.setPreferredScrollableViewportSize(new Dimension(400, 200));
		JScrollPane spFichas = new JScrollPane(jtbFichas);

		this.getContentPane().setLayout (new FlowLayout());
		this.getContentPane().add (btnClear);
		this.getContentPane().add (spFichas);

		this.setTitle("Prueba MVC");
		this.setSize(600,400);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);		
	}
	
	void addClearListener(ActionListener cal) 
	{
        btnClear.addActionListener(cal);
    }
     
  	void setTabla() {
              jtbFichas.setModel(m_model.getModelo());
              // esta linea provoca el siguiente error
            //Exception in thread "AWT-EventQueue-0" java.lang.NullPointerException
           // at Vista.setTabla(Vista.java:35)
    }  
}
