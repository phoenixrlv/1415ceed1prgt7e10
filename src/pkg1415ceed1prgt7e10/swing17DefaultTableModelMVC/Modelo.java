/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing17DefaultTableModelMVC;

import javax.swing.table.DefaultTableModel;

/**
 * Fichero: Modelo.java
 * @date 09-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * Fuente:
 * http://www.forosdelweb.com/f45/problema-diseno-mvc-con-jtable-tablemodel-901295/
 */
public class Modelo {

  private DefaultTableModel defaulttablemodel;

  public Modelo() {

    Object[][] data = {
      {"Juan", "carpintero", "Alicante"},
      {"Marcos", "Abogado", "Pontevedra"},
      {"Mercedes", "Doctora", "Cuenca"}
    };
    String[] cols = {"Col1", "Col2", "Col3"};
    this.defaulttablemodel = new DefaultTableModel(data, cols);
  }

  public DefaultTableModel getModelo() {
    return defaulttablemodel;
  }

  public void setModelo() {
    Object[][] data = {
      {"Rodriguez", "carpintero", "Alicante"},
      {"Garcia", "Abogado", "Pontevedra"},
      {"Perez", "Doctora", "Cuenca"}
    };
    String[] cols = {"Col1", "Col2", "Col3"};
    defaulttablemodel = new DefaultTableModel(data, cols);
  }
}
