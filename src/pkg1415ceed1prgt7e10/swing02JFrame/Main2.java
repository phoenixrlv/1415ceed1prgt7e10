/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swing02JFrame;

import javax.swing.JFrame;

/**
 * Fichero: Main1.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 17-dic-2014
 */
public class Main2 extends JFrame {

  /**
   * @param args the command line arguments
   */
  Main2() {
    setBounds(0, 0, 400, 300);
    setTitle("Ejemplo JFrame");
    setVisible(true);
  }

  public static void main(String[] args) {
    // TODO code application logic here
    new Main2();
  }

}
