/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jpannel2;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
class TabOnFocusChangeListener implements ChangeListener {

  private MainFrame jframe;

  TabOnFocusChangeListener(MainFrame pjframe) {
    this.jframe = pjframe;
  }

  @Override
  public void stateChanged(ChangeEvent e) {
    jframe.tabsOnFocus(e);
  }
}
