/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing16TableModelListener;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 *  * Ejemplo de uso del TableModelListener.
 * Una tabla de 3 filas y 3 columnas con numeros, de forma que la ultima columna es la suma
 * de las dos columnas anteriores y la ultima fila es la suma de las dos filas anteriores.
 * Solo son editables las celdas que no son de la ultima fila ni de la ultima columna.
 * Se usa TableModelListener para actualizar las sumas cuando el usuario modifica el valor de
 * alguna de las celdas.
 * Fichero: Main.java
 * @date 08-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * Fuente:
 * http://chuwiki.chuidiang.org/index.php?title=Ejemplo_de_uso_de_TableModelListener
 */



public class Main {

    public static void main(String[] args) {
        new Main();
    }

    public Main() {
        JTable tabla = creaVentanaConTabla();
        anadeListenerAlModelo(tabla);
    }

    /**
     * Se añade el listener al modelo y se llama a actualizaSumas(), que es el método 
     * encargado de actualizar las sumas de las celdas no editables.
     */
    private void anadeListenerAlModelo(JTable tabla) {
        tabla.getModel().addTableModelListener(new TableModelListener() {

            public void tableChanged(TableModelEvent evento) {
                actualizaSumas(evento);
            }

            /**
     * Cuando cambie el valor de alguna celda, el JTable avisará a los listener y
     * nuestro listener llamará a este método.
     */
          private void actualizaSumas(TableModelEvent evento) {
            
        // Solo se trata el evento UPDATE, correspondiente al cambio de valor
        // de una celda.
        if (evento.getType() == TableModelEvent.UPDATE) {

            // Se obtiene el modelo de la tabla y la fila/columna que han cambiado.
            TableModel modelo = ((TableModel) (evento.getSource()));
            int fila = evento.getFirstRow();
            int columna = evento.getColumn();

            // Los cambios en la ultima fila y columna se ignoran.
            // Este return es necesario porque cuando nuestro codigo modifique
            // los valores de las sumas en esta fila y columna, saltara nuevamente
            // el evento, metiendonos en un bucle recursivo de llamadas a este
            // metodo.
            if (fila == 2 || columna == 2) {
                return;
            }

            try {
                // Se actualiza la suma en la ultima columna de la fila que ha
                // cambiado.
                double valorPrimeraColumna = ((Number) modelo.getValueAt(fila,
                        0)).doubleValue();
                double valorSegundaColumna = ((Number) modelo.getValueAt(fila,
                        1)).doubleValue();
                modelo.setValueAt(valorPrimeraColumna + valorSegundaColumna,
                        fila, 2);

                // Se actualiza la suma en la ultima fila de la columna que ha
                // cambiado.
                double valorPrimeraFila = ((Number) modelo.getValueAt(0,
                        columna)).doubleValue();
                double valorSegundaFila = ((Number) modelo.getValueAt(1,
                        columna)).doubleValue();
                modelo.setValueAt(valorPrimeraFila + valorSegundaFila, 2,
                        columna);

                // Se actualiza la ultima fila y columna, que tiene la suma total
                // de todas las celdas.
                double valorPrimeraFilaUltimaColumna = ((Number) modelo
                        .getValueAt(0, 2)).doubleValue();
                double valorSegundaFilaUltimaColumna = ((Number) modelo
                        .getValueAt(1, 2)).doubleValue();
                modelo.setValueAt(valorPrimeraFilaUltimaColumna
                        + valorSegundaFilaUltimaColumna, 2, 2);
                
            } catch (NullPointerException e) {
                // La celda que ha cambiado esta vacia.
            }
        }

            
          }
        });
    }

  private JTable creaVentanaConTabla() {

        JFrame ventana = new JFrame();
        JTable tabla = new JTable(new MiModelo());
        tabla.setDefaultRenderer(Double.class, new MiRender());
        JScrollPane scroll = new JScrollPane(tabla);
        ventana.getContentPane().add(scroll);
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.pack();
        ventana.setVisible(true);
        return tabla;
    }
  }
