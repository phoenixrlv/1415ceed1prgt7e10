/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing16TableModelListener;

import javax.swing.table.DefaultTableModel;

/**
 * Fichero: MiModelo.java
 * @date 08-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class MiModelo extends DefaultTableModel {
        
        /**
         * Se carga con tres filas y tres columnas con numeros.
         */
        public MiModelo() {
            super(new Object[][] { { 1, 1, 2 }, { 1, 1, 2 }, { 2, 2, 4 } },
                    new String[] { "A", "B", "C" });
        }

        /**
         * El contenido de las celdas sera Double.
         */
        @Override
        public Class<?> getColumnClass(int arg0) {
            return Double.class;
        }

        /**
         * Se hacen no editables la ultima fila y columna.
         * Es decir, es editable si no es la fila 2 ni la columna 2.
         */
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return fila < 2 && columna < 2;
        }
    }