/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package swing16TableModelListener;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

/**
 * Un Render para mostrar de un color distinto las celdas no editables.
 * Fichero: MiRender.java
 * @date 08-mar-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class MiRender extends DefaultTableCellRenderer {
                public Component getTableCellRendererComponent(JTable table,
                Object value, boolean isSelected, boolean hasFocus, int row,
                int column) {
            
            // Se llama al metodo de la clase padre para que ponga bien los
            // colores de celda con/sin foco, celda de/seleccionada, dibuje el 
            // valor de la celda, etc.
            super.getTableCellRendererComponent(table, value, isSelected,
                    hasFocus, row, column);
            
            // Y nosotros solo cambiamos el color de fondo.
            if (row == 2 || column == 2) {
                // Si es ultima fila o columna, es decir, celda no editable,
                // color amarillo.
                setBackground(Color.YELLOW);
            } else {
                // Si es editable, color blanco.
                setBackground(Color.WHITE);
            }
            
            // DefaultTableCellRenderer hereda de JLabel y es este el JLabel que
            // devolvemos.
            return this;
        }
    }

