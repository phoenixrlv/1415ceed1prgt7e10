package pkg1415ceed1prgt7e10.mvc;

/**
 * Fichero: Main.java
 *
 * @date 16-feb-2014
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */

/* Fuente:
 https://www.fdi.ucm.es/profesor/jpavon/poo/2.14.Main.pdf
 */
public class Main {

    public static void main(String[] args) {
        // el modelo:
        ModeloEurosPesetas modelo = new ModeloEurosPesetas();
        // la vista:
        //Vista vista = new VistaTexto();
        Vista vista = new VistaGrafica();
        // y el control:
        Controlador control = new Controlador(vista, modelo);
        // configura la vista
        vista.setControlador(control);
        // y arranca la interfaz (vista):
        vista.arranca();
    }
}
/*
 Discusión final

 ¿Qué hay que cambiar en el modelo y el control para
 utilizar la vista textual en vez de la gráfica?
 Respuesta: Nada

 ¿Qué hay que cambiar en el programa principal?
 Respuesta:
 En el main() hay que cambiar
 Vista vista = new VistaGrafica();
 por
 Vista vista = new InterfazTextualConversor();
 * */
