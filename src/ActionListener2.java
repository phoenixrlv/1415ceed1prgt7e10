
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ActionListener2 extends JFrame {

    private JButton b;

    public ActionListener2() {
        super("ActionListener2");
        b = new JButton("Pulsame");
        ActionListener2a elListener = new ActionListener2a();
        b.addActionListener(elListener);
        add(b);

        pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
    }

    private void pulsado() {
        b.setText("Pulsado");
    }

    public static void main(String[] argc) {
        new ActionListener2().setVisible(true);
    }

    class ActionListener2a implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            pulsado();
        }
    }
}
