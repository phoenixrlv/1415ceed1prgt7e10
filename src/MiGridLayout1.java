
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Paco Aldarias<paco.aldarias@gmail.com>
 * @author 04-feb-2015
 */
public class MiGridLayout1 extends JFrame {

   private JLabel l1;
   private JLabel l2;
   private JLabel l3;
   private JLabel l4;
   private GridLayout gl;

   public MiGridLayout1() {

      l1 = new JLabel("Dato1");
      l2 = new JLabel("Dato2");
      l3 = new JLabel("Dato3");
      l4 = new JLabel("Dato4");

      add(l1);
      add(l2);
      add(l3);
      add(l4);

      gl = new GridLayout();
      gl.setRows(2); // Filas
      gl.setHgap(10);
      gl.setColumns(2); // Columnas
      gl.setVgap(10);

      setLayout(gl);

      //algunas configuraciones de la ventana
      //pack();
      setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
      setLocationRelativeTo(null); // Centrar
      setSize(100, 100);
      setVisible(true);

   }

   public static void main(String[] argc) {
      new MiGridLayout1();
   }

}
