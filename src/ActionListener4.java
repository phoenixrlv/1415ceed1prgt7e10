
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

public class ActionListener4 extends JFrame {

    private JButton b;
    private int contador = 1;

    public ActionListener4() {
        super("ActionListener 4");
        b = new JButton("Pulsame 0");

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                b.setText("Pulsado " + contador);
                contador++;
            }
        });

        add(b);

        pack();
        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
    }

    public static void main(String[] argc) {
        new ActionListener4().setVisible(true);
    }
}
