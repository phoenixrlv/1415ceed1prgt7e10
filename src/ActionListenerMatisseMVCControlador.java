
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @date 16-feb-2015 Fichero ActionListenerMatisseMVCControlador.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class ActionListenerMatisseMVCControlador implements ActionListener {

    ActionListenerMatisseMVCControlador(ActionListenerMatisseMVCVista v) {
        v.configurarBotones(this);
        v.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        System.exit(0);
    }

}
