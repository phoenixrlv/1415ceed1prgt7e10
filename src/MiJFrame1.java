
import javax.swing.JFrame;
import javax.swing.JLabel;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

public class MiJFrame1 {

    private JFrame jf;
    private JLabel jl;

    public MiJFrame1() {
        initComponents();
    }

    public void initComponents() {
        jf = new JFrame();
        jl = new JLabel();
        jf.add(jl);
        jl.setText("Hola Mundo");

        jf.setSize(200, 200);
        jf.setVisible(true);
        jf.setDefaultCloseOperation(jf.EXIT_ON_CLOSE); // Cierra Aplicacion
        jf.setLocationRelativeTo(null); // Centrar
    }

    public static void main(String args[]) {
        new MiJFrame1();
    }

}
