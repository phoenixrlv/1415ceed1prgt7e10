
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
/*
 Aplicación simple con una etiqueta y sin JPanel
 */

public class MiJTextField extends JFrame {

    private JTextField t;
    private JPanel p;

    MiJTextField() {
        initComponents();
    }

    private void initComponents() {

        p = new JPanel();
        p.setBackground(Color.yellow);
        add(p);

        t = new JTextField();
        t.setText("TextField");
        t.setColumns(15);
        p.add(t);

        setLocationRelativeTo(null); // Centrar
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Cierra Aplicacion
        setSize(250, 100);
        setVisible(true);
    }

    public static void main(String args[]) {
        new MiJTextField();
    }

}
